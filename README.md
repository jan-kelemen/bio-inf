# bio-inf
Project repository for [Bioinformathics](http://www.fer.unizg.hr/predmet/bio)
course held at Faculty of Electrical Engineering and Computing, University of Zagreb.

## Title
Construction of binary wavelet trees using RRR structure.

## Team members
Jan Kelemen, 0036479753  
Marina Brebrić, 0036466293  
Leonard Volarić Horvat, 0036478030  

## Repository contents
* *doc* directory contains the project documentation. Check *documentation.pdf* file for
 more details about the project and measurement results
* *src-doc* directory contains the code documentation. Check *source_documentation.pdf* file
 for documentation about used classes and functions
* *src* directory contains C++ source code of the project
* *tests* directory contains synthetic tests and FASTA tests. 

## Compiling on Windows
*src/msvc14* directory contains the Microsoft Visual Studio 2015 solution file, open
this file, configure it to Release x64 build configuration and build the project 
by selecting Build > Build Solution or pressing Ctrl + Shift + B.
Built executable will be located in *src/msvc14/x64/Release* directory and named *bwt-rrr.exe*

## Compiling on Linux
Position yourself in src/directory and enter "gcc -std=c++14 \*.cpp" in the terminal.
This command will compile the project to a *a.out* executable.

## Run instructions
Program expects 3 arugments. First is the filename of the sequence in FASTA format. Second is
the filename of the commands to be executed. Third is the filename of the output file.
For example, "bwt-rrr.exe s.txt c.txt o.txt" will read the sequence from *s.txt* file and commands
from *c.txt* file, results of which will be printed in *o.txt* file.
