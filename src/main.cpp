#include <chrono>
#include <cstdio>
#include <fstream>
#include <iostream>

#include "WaveletTree.h"
#include "bioinf_utility.h"

void print_memory_usage(std::ostream& stream);

int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cout << "Invalid number of arguments\n";
        std::cout << "First arugment is the flename of the sequence file\n";
        std::cout << "Second argument is the filename of the commands file\n";
        std::cout << "Third argument is the filename of the output file\n";
        return EXIT_FAILURE;
    }

    try {

        bioinf::Statistics stats;

        auto start = std::chrono::high_resolution_clock::now();
        auto sequence = bioinf::parse_fasta(argv[1]);
        bioinf::calculate_memory_usage();
        auto tree = bioinf::WaveletTree(sequence);
        auto elapsed = std::chrono::high_resolution_clock::now() - start;
        stats.tree_construction_time = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

        //sequence.clear(); //clear the input sequence so it doesn't use memory
        print_memory_usage(std::cout);

        std::ifstream commands(argv[2]);
        std::string results;
        std::string line;
        while (std::getline(commands, line)) {
            try {
                switch (line[0]) {
                    // rank operation
                    case 'r':
                    {
                        char c;
                        size_t i;
                        sscanf(line.c_str(), "rank %c %zu", &c, &i);

                        start = std::chrono::high_resolution_clock::now();
                        auto res = tree.rank(c, i);
                        elapsed = std::chrono::high_resolution_clock::now() - start;
                        auto microseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
                        stats.rank_times.push_back(microseconds);

                        results += std::to_string(res);
                        results += '\n';
                        break;
                    }
                    // select operation
                    case 's':
                    {
                        char c;
                        size_t i;
                        sscanf(line.c_str(), "select %c %zu", &c, &i);

                        start = std::chrono::high_resolution_clock::now();
                        auto res = tree.select(c, i);
                        elapsed = std::chrono::high_resolution_clock::now() - start;
                        auto microseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
                        stats.select_times.push_back(microseconds);

                        results += std::to_string(res);
                        results += '\n';
                        break;
                    }
                    // access operation
                    case 'a':
                    {
                        size_t i;
                        sscanf(line.c_str(), "access %zu", &i);

                        start = std::chrono::high_resolution_clock::now();
                        auto res = tree.access(i);
                        elapsed = std::chrono::high_resolution_clock::now() - start;
                        auto microseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed).count();
                        stats.access_times.push_back(microseconds);

                        results += res;
                        results += '\n';
                        break;
                    }
                    // memory operation
                    case 'M':
                    {
                        print_memory_usage(std::cout);
                        break;
                    }
                    // statistics operation
                    case 'S':
                    {
                        auto avgs = bioinf::calculate_statistics(stats);
                        std::cout << "Tree construction time (us): " << std::get<0>(avgs) << '\n';
                        std::cout << "Average access time (ns): " << std::get<1>(avgs) << '\n';
                        std::cout << "Average rank time (ns): " << std::get<2>(avgs) << '\n';
                        std::cout << "Average select time (ns): " << std::get<3>(avgs) << '\n';
                        break;
                    }
                    // exit operation
                    case 'E':
                    {
                        break;
                    }
                    default:
                    {
                        std::cerr << "Invalid command: " << line << '\n';
                        break;
                    }
                }
            }
            catch (std::exception& e) {
                std::cerr << "Command, " << line << ", execution failed with exception " << e.what() << '\n';
            }
        }

        std::ofstream output(argv[3]);
        output << results;
    }
    catch (std::exception& e) {
        std::cerr << "Execution failed with exception: " << e.what() << '\n';
        return EXIT_FAILURE;
    }

}

void print_memory_usage(std::ostream& stream) {
    auto memory = bioinf::calculate_memory_usage();
    stream << "Memory usage (MB): ";
    stream << memory.first << " (process), ";
    stream << memory.second << " (tree)\n";
}
