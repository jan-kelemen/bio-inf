#include "RRRTable.h"

#include <algorithm>
#include <string>

#include "bioinf_utility.h"

namespace bioinf {
    namespace {
        RRRTable::rank_vector_t calculate_rank_vector(std::string const& block_str) {
            auto rv = RRRTable::rank_vector_t();
            rv.reserve(block_str.size());

            auto rank = 0;
            for (auto b : block_str) {
                if (b == '1') {
                    ++rank;
                }
                rv.push_back(rank);
            }

            return rv;
        }

        RRRTable::block_t convert_to_binary(std::string const& block_str) {
            RRRTable::block_t rv = 0;
            RRRTable::block_t mask = 1;

            for (auto it = block_str.rbegin(); it != block_str.rend(); ++it) {
                if (*it == '1') {
                    rv |= mask;
                }
                mask <<= 1;
            }

            return rv;
        }
    }

    RRRTable::table_t RRRTable::table_;
    std::array<int8_t, sizeof(size_t) * 8 + 1> RRRTable::bits_for_offset_;
    int8_t RRRTable::block_size_ = 0;

    void RRRTable::initialize_for_block_size(int8_t block_size) {
        table_.clear();

        block_size_ = block_size;
        auto table_size = block_size + 1;

        table_.resize(table_size);

        for (auto i = 0; i < table_size; ++i) {
            auto block_str = std::string(block_size, '0');
            std::for_each(block_str.end() - i, block_str.end(), [](char& c) { c = '1'; });

            do {
                table_[i].emplace_back(convert_to_binary(block_str), calculate_rank_vector(block_str));
            } while (std::next_permutation(block_str.begin(), block_str.end()));

            auto bits = static_cast<int8_t>(std::ceil(std::log2(binomial(block_size, i))));
            bits_for_offset_[i] = bits == 0 ? 1 : bits;
        }
    }

    size_t RRRTable::offset_of(block_t const& block, class_t const& value) {
        auto& blocks = table_[value];
        for (size_t i = 0; i < blocks.size(); ++i) {
            if (blocks[i].first == block) {
                return i;
            }
        }
        throw std::logic_error("Block doesn't exist");
    }

    int8_t RRRTable::bits_for_offset(uint8_t const& b, class_t const& c) {
        return bits_for_offset_[c];
    }

    int8_t RRRTable::rank_for(class_t const& c, size_t const& offset, int8_t const& block_size, size_t const& position) {
        return table_[c][offset].second[block_size_ - block_size + position];
    }

    int8_t RRRTable::index_for_rank0(class_t const& c, size_t const& offset, int8_t const& block_size, int8_t const& rank) {
        auto& ranks = table_[c][offset].second;
        if (rank - block_size_ > 0) {
            throw - 1;
        }

        auto compensation = block_size_ - block_size;
        int low = compensation;
        int high = ranks.size() - 1;
        while (low <= high) {
            auto mid = (low + high) / 2;
            auto value = mid - ranks[mid] + 1 - compensation;
            if (value >= rank) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }

        return low - compensation;
    }

    int8_t RRRTable::index_for_rank1(class_t const& c, size_t const& offset, int8_t const& block_size, int8_t const& rank) {
        auto& ranks = table_[c][offset].second;
        auto begin = ranks.begin() + block_size_ - block_size;

        int8_t compensation = block_size_ != block_size ? *(begin - 1) : 0;

        auto it = std::lower_bound(begin, ranks.end(), rank + compensation);

        if (it == ranks.end() && *(it - 1) != rank) { throw std::logic_error("Rank doesn't exist"); }

        //block cant be larger than 64 bits, int8_t is ok
        return static_cast<int8_t>(std::distance(begin, it));
    }
}

