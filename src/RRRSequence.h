#ifndef RRR_SEQUENCE_H
#define RRR_SEQUENCE_H

#include <string>
#include <vector>

#include "RRRTable.h"

namespace bioinf {

    /**
     * \brief WaveletNode is a node of a binary wavelet tree implemented using RRR structure.
     *
     * \author Leonard Volaric Horvat
     * \version 1.0
     */
    class RRRSequence {
        /**
         * \brief Converts the RRR sequence to text.
         * \param[in] sequence The sequence.
         * \return Textual representation of the coded RRR sequence.
         */
        friend std::string to_string(RRRSequence const& sequence);
    public:
        // - - - Constructors
        
        /**
         * \brief Class constructor, constructs a empty RRR sequence.
         * \since alpha
         */
        RRRSequence() = default;

        /**
         * \brief Class constructor, constructs a RRR sequence from the given string.
         * \param[in] sequence The sequence for which the rrr sequence is created.
         * \since alpha
         */
        explicit RRRSequence(std::string const& sequence);

        /**
         * \brief Copy constructor.
         * \since 1.0
         */
        RRRSequence(RRRSequence const&) = default;

        /**
         * \brief Move constructor
         * \since 1.0
         */
        RRRSequence(RRRSequence&&) = default;

        // - - - Operator overloads

        /**
         * \brief Copy assignment.
         * \since 1.0
         */
        RRRSequence& operator=(RRRSequence const&) = default;

        /**
         * \brief Move assignment.
         * \since 1.0
         */
        RRRSequence& operator=(RRRSequence&&) = default;

        // - - - Member functions

        /**
         * \brief Calculates the rank of 0 in the range [0, index].
         * \param[in] index The index.
         * \return Number of occurrences of 0 in the node in specified range.
         * \since alpha
         */
        size_t rank0(size_t index) const;

        /**
         * \brief Calculates the rank of 1 in the range [0, index].
         * \param[in] index The index.
         * \return Number of occurrences of 1 in the node in specified range.
         * \since alpha
         */
        size_t rank1(size_t index) const;

        /**
         * \brief Calculates the index at which 0 appears in the node for the count-th time.
         * \param[in] count The count.
         * \return Index of the count-th appearence.
         * \since alpha
         */
        size_t select0(size_t count) const;

        /**
         * \brief Calculates the index at which 1 appears in the node for the count-th time.
         * \param[in] count The count.
         * \return Index of the count-th appearence.
         * \since alpha
         */
        size_t select1(size_t count) const;

        /**
         * \brief Returns the character of the sequence at specified index.
         * \param[in] index The index.
         * \return The bit.
         * \since alpha
         */
        char access(size_t index) const;

        //TODO: remove these methods (only used for testing).
        uint8_t block_length() const;
        uint8_t blocks_in_superblock() const;
        uint8_t class_length() const;
        size_t sequence_length() const;

    private:
        uint8_t block_length_;
        uint8_t blocks_in_superblock_;
        uint8_t class_length_;
        size_t total_length_;

        size_t sequence_length_;
        size_t total_blocks_;

        std::vector<int64_t> coded_sequence_;

        //first number is rank of all previous blocks
        //second number is the bit at which first offset of the superblock begins
        std::vector<std::pair<size_t, size_t>> superblocks_;

        //Calculates the bit sizes and similar values.
        void calculate_lengths(size_t length);

        //Returns the index of the superblock for select0 operation
        size_t select0_superblock(size_t count) const;
    };
}
#endif // !RRR_SEQUENCE_H
