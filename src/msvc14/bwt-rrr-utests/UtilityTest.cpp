#include "stdafx.h"
#include "CppUnitTest.h"

#include "..\..\bioinf_utility.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace bioinf;

namespace bwtrrrutests {
    TEST_CLASS(UtilityTest) {
public:

    TEST_METHOD(TestPopcount) {
        Assert::AreEqual<int8_t>(0, popcount(0), L"Invalid popcount", LINE_INFO());

        Assert::AreEqual<int8_t>(1, popcount(1), L"Invalid popcount", LINE_INFO());
        Assert::AreEqual<int8_t>(1, popcount(2), L"Invalid popcount", LINE_INFO());
        Assert::AreEqual<int8_t>(1, popcount(4), L"Invalid popcount", LINE_INFO());

        Assert::AreEqual<int8_t>(2, popcount(3), L"Invalid popcount", LINE_INFO());
        Assert::AreEqual<int8_t>(2, popcount(5), L"Invalid popcount", LINE_INFO());
        Assert::AreEqual<int8_t>(2, popcount(6), L"Invalid popcount", LINE_INFO());

        Assert::AreEqual<int8_t>(3, popcount(7), L"Invalid popcount", LINE_INFO());
    }

    TEST_METHOD(TestFactorial) {
        Assert::AreEqual<size_t>(1, factorial(1), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(2, factorial(2), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(6, factorial(3), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(24, factorial(4), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(120, factorial(5), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(720, factorial(6), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(5040, factorial(7), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(40320, factorial(8), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(362880, factorial(9), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(3628800, factorial(10), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(39916800, factorial(11), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(479001600, factorial(12), L"Invalid factorial value", LINE_INFO());

#ifdef _WIN64
        //test will fail before on smaller numbers so it's ok to use fac(n-1) to check correctnes
        Assert::AreEqual<size_t>(13 * factorial(12), factorial(13), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(14 * factorial(13), factorial(14), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(15 * factorial(14), factorial(15), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(16 * factorial(15), factorial(16), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(17 * factorial(16), factorial(17), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(18 * factorial(17), factorial(18), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(19 * factorial(18), factorial(19), L"Invalid factorial value", LINE_INFO());
        Assert::AreEqual<size_t>(20 * factorial(19), factorial(20), L"Invalid factorial value", LINE_INFO());
#endif // _WIN64
    }

    TEST_METHOD(TestBinomial) {
        Assert::AreEqual<size_t>(20, binomial(6, 3), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(15, binomial(6, 2), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(7, binomial(7, 1), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(3, binomial(3, 2), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(15, binomial(6, 4), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(1, binomial(11, 0), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(1, binomial(8, 8), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(9, binomial(9, 1), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(210, binomial(10, 6), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(1, binomial(0, 0), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(35, binomial(7, 3), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(55, binomial(11, 9), L"Invalid binomial value", LINE_INFO());
        Assert::AreEqual<size_t>(9, binomial(9, 8), L"Invalid binomial value", LINE_INFO());
    }

    TEST_METHOD(TestExtractBits) {
        std::vector<uint64_t> val = {0xeebecae8afb72355, 0x49f4b01bf1d08ca8};

        //first byte from 0
        Assert::AreEqual<uint8_t>(1, extract_bits<uint8_t>(val.begin(), 0, 1), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(3, extract_bits<uint8_t>(val.begin(), 0, 2), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(7, extract_bits<uint8_t>(val.begin(), 0, 3), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(14, extract_bits<uint8_t>(val.begin(), 0, 4), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(29, extract_bits<uint8_t>(val.begin(), 0, 5), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(59, extract_bits<uint8_t>(val.begin(), 0, 6), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(119, extract_bits<uint8_t>(val.begin(), 0, 7), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(238, extract_bits<uint8_t>(val.begin(), 0, 8), nullptr, LINE_INFO());
        //last byte from 0
        Assert::AreEqual<uint8_t>(0, extract_bits<uint8_t>(val.begin(), 56, 1), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(1, extract_bits<uint8_t>(val.begin(), 56, 2), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(2, extract_bits<uint8_t>(val.begin(), 56, 3), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(5, extract_bits<uint8_t>(val.begin(), 56, 4), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(10, extract_bits<uint8_t>(val.begin(), 56, 5), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(21, extract_bits<uint8_t>(val.begin(), 56, 6), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(42, extract_bits<uint8_t>(val.begin(), 56, 7), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(85, extract_bits<uint8_t>(val.begin(), 56, 8), nullptr, LINE_INFO());

        //first byte from 1
        Assert::AreEqual<uint8_t>(0, extract_bits<uint8_t>(val.begin(), 64, 1), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(1, extract_bits<uint8_t>(val.begin(), 64, 2), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(2, extract_bits<uint8_t>(val.begin(), 64, 3), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(4, extract_bits<uint8_t>(val.begin(), 64, 4), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(9, extract_bits<uint8_t>(val.begin(), 64, 5), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(18, extract_bits<uint8_t>(val.begin(), 64, 6), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(36, extract_bits<uint8_t>(val.begin(), 64, 7), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(73, extract_bits<uint8_t>(val.begin(), 64, 8), nullptr, LINE_INFO());

        //last byte from 1
        Assert::AreEqual<uint8_t>(1, extract_bits<uint8_t>(val.begin(), 120, 1), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(2, extract_bits<uint8_t>(val.begin(), 120, 2), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(5, extract_bits<uint8_t>(val.begin(), 120, 3), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(10, extract_bits<uint8_t>(val.begin(), 120, 4), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(21, extract_bits<uint8_t>(val.begin(), 120, 5), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(42, extract_bits<uint8_t>(val.begin(), 120, 6), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(84, extract_bits<uint8_t>(val.begin(), 120, 7), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(168, extract_bits<uint8_t>(val.begin(), 120, 8), nullptr, LINE_INFO());

        //over the edge
        Assert::AreEqual<uint8_t>(0, extract_bits<uint8_t>(val.begin(), 60, 1), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(1, extract_bits<uint8_t>(val.begin(), 60, 2), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(2, extract_bits<uint8_t>(val.begin(), 60, 3), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(5, extract_bits<uint8_t>(val.begin(), 60, 4), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(10, extract_bits<uint8_t>(val.begin(), 60, 5), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(21, extract_bits<uint8_t>(val.begin(), 60, 6), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(42, extract_bits<uint8_t>(val.begin(), 60, 7), nullptr, LINE_INFO());
        Assert::AreEqual<uint8_t>(84, extract_bits<uint8_t>(val.begin(), 60, 8), nullptr, LINE_INFO());
    }
    };
}