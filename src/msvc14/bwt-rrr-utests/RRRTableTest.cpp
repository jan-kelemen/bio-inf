#include "stdafx.h"
#include "CppUnitTest.h"

#include "..\..\RRRTable.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace bioinf;

namespace bwtrrrutests {
    TEST_CLASS(RRRTableTest3) {
public:
    RRRTableTest3() {
        RRRTable::initialize_for_block_size(3);
    }

    TEST_METHOD(TestOffsetOf) {
        Assert::AreEqual<size_t>(0, RRRTable::offset_of(0, 0), L"Block 000 has invalid offset", LINE_INFO());

        Assert::AreEqual<size_t>(0, RRRTable::offset_of(1, 1), L"Block 001 has invalid offset", LINE_INFO());
        Assert::AreEqual<size_t>(1, RRRTable::offset_of(2, 1), L"Block 010 has invalid offset", LINE_INFO());
        Assert::AreEqual<size_t>(2, RRRTable::offset_of(4, 1), L"Block 100 has invalid offset", LINE_INFO());

        Assert::AreEqual<size_t>(0, RRRTable::offset_of(3, 2), L"Block 011 has invalid offset", LINE_INFO());
        Assert::AreEqual<size_t>(1, RRRTable::offset_of(5, 2), L"Block 101 has invalid offset", LINE_INFO());
        Assert::AreEqual<size_t>(2, RRRTable::offset_of(6, 2), L"Block 110 has invalid offset", LINE_INFO());

        Assert::AreEqual<size_t>(0, RRRTable::offset_of(7, 3), L"Block 111 has invalid offset", LINE_INFO());
    }

    TEST_METHOD(TestOffsetOfNonexistingBlock) {
        auto fun = []() { return RRRTable::offset_of(8, 1); };
        Assert::ExpectException<std::logic_error>(fun, L"Nonexisting block was found", LINE_INFO());
    }

    TEST_METHOD(TestBitsForOffset) {
        Assert::AreEqual<int8_t>(1, RRRTable::bits_for_offset(3, 0), L"Bit count doesn't match", LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::bits_for_offset(3, 1), L"Bit count doesn't match", LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::bits_for_offset(3, 2), L"Bit count doesn't match", LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::bits_for_offset(3, 3), L"Bit count doesn't match", LINE_INFO());
    }

    TEST_METHOD(TestIndexForRank0) {
        //000
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(0, 0, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(0, 0, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank0(0, 0, 3, 3), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(0, 0, 2, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(0, 0, 2, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(0, 0, 1, 1), nullptr, LINE_INFO());

        //001
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(1, 0, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(1, 0, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(1, 0, 2, 1), nullptr, LINE_INFO());

        //010
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(1, 1, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank0(1, 1, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(1, 1, 2, 1), nullptr, LINE_INFO());

        //100
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(1, 2, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank0(1, 2, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank0(1, 2, 2, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank0(1, 2, 2, 2), nullptr, LINE_INFO());

        //011
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank0(2, 0, 3, 1), nullptr, LINE_INFO());
    }

    TEST_METHOD(TestIndexForRank1) {
        //001
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank1(1, 0, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(1, 0, 2, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(1, 0, 1, 1), nullptr, LINE_INFO());

        //010
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(1, 1, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(1, 1, 2, 1), nullptr, LINE_INFO());

        //100
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(1, 2, 3, 1), nullptr, LINE_INFO());

        //011
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(2, 0, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank1(2, 0, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(2, 0, 2, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(2, 0, 2, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(2, 0, 1, 1), nullptr, LINE_INFO());

        //101
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(2, 1, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank1(2, 1, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(2, 1, 2, 1), nullptr, LINE_INFO());

        //110
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(2, 2, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(2, 2, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(2, 2, 2, 1), nullptr, LINE_INFO());

        //111
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(3, 0, 3, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(3, 0, 3, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(2, RRRTable::index_for_rank1(3, 0, 3, 3), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(3, 0, 2, 1), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(1, RRRTable::index_for_rank1(3, 0, 2, 2), nullptr, LINE_INFO());
        Assert::AreEqual<int8_t>(0, RRRTable::index_for_rank1(3, 0, 1, 1), nullptr, LINE_INFO());
    }
    };
}