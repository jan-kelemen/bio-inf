#include "stdafx.h"
#include "CppUnitTest.h"

#include <string>

#include "../../WaveletTree.h"
#include "../../WaveletNode.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace bioinf;

namespace bwtrrrutests {
    TEST_CLASS(WaveletTreeTest) {
public:

    TEST_METHOD(TestRank) {
        auto tree = WaveletTree("SAMSUNG GALAXY ALPHA");
        Assert::AreEqual<size_t>(0, tree.rank(' ', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank(' ', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank(' ', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank(' ', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank(' ', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank(' ', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank(' ', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank(' ', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank(' ', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank(' ', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank(' ', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank(' ', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank(' ', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank(' ', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(2, tree.rank(' ', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(2, tree.rank(' ', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank(' ', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank(' ', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(2, tree.rank(' ', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(2, tree.rank(' ', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('A', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(1, tree.rank('A', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('A', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(1, tree.rank('A', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(1, tree.rank('A', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(1, tree.rank('A', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(1, tree.rank('A', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('A', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('A', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(2, tree.rank('A', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('A', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(3, tree.rank('A', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(3, tree.rank('A', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(3, tree.rank('A', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(3, tree.rank('A', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(4, tree.rank('A', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(4, tree.rank('A', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(4, tree.rank('A', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(4, tree.rank('A', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(5, tree.rank('A', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('G', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('G', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('G', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('G', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('G', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('G', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(1, tree.rank('G', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('G', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(2, tree.rank('G', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(2, tree.rank('G', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('G', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank('G', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('G', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(2, tree.rank('G', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(2, tree.rank('G', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(2, tree.rank('G', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('G', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank('G', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(2, tree.rank('G', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(2, tree.rank('G', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('H', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('H', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('H', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('H', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('H', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('H', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank('H', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('H', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('H', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('H', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('H', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(0, tree.rank('H', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('H', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(0, tree.rank('H', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(0, tree.rank('H', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('H', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('H', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(0, tree.rank('H', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('H', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('H', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('L', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('L', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('L', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('L', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('L', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('L', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank('L', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('L', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('L', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('L', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('L', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('L', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('L', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('L', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('L', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('L', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('L', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank('L', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(2, tree.rank('L', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(2, tree.rank('L', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('M', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('M', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('M', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(1, tree.rank('M', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(1, tree.rank('M', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(1, tree.rank('M', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(1, tree.rank('M', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('M', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('M', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('M', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('M', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('M', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('M', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('M', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('M', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('M', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('M', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('M', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('M', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('M', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('N', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('N', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('N', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('N', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('N', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(1, tree.rank('N', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(1, tree.rank('N', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('N', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('N', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('N', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('N', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('N', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('N', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('N', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('N', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('N', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('N', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('N', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('N', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('N', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('P', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('P', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('P', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('P', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('P', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('P', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank('P', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('P', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('P', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('P', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('P', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(0, tree.rank('P', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('P', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(0, tree.rank('P', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(0, tree.rank('P', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('P', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('P', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('P', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('P', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('P', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(1, tree.rank('S', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(1, tree.rank('S', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('S', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(2, tree.rank('S', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(2, tree.rank('S', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(2, tree.rank('S', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(2, tree.rank('S', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(2, tree.rank('S', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(2, tree.rank('S', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(2, tree.rank('S', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('S', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank('S', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('S', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(2, tree.rank('S', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(2, tree.rank('S', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(2, tree.rank('S', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(2, tree.rank('S', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(2, tree.rank('S', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(2, tree.rank('S', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(2, tree.rank('S', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('U', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('U', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('U', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('U', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(1, tree.rank('U', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(1, tree.rank('U', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(1, tree.rank('U', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('U', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('U', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(1, tree.rank('U', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('U', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('U', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('U', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('U', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('U', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('U', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('U', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('U', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('U', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('U', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('X', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('X', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('X', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('X', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('X', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('X', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank('X', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('X', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('X', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('X', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('X', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(0, tree.rank('X', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('X', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('X', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('X', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('X', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('X', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('X', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('X', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('X', 19), nullptr, LINE_INFO()); //A

        Assert::AreEqual<size_t>(0, tree.rank('Y', 0), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('Y', 1), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('Y', 2), nullptr, LINE_INFO()); //M
        Assert::AreEqual<size_t>(0, tree.rank('Y', 3), nullptr, LINE_INFO()); //S
        Assert::AreEqual<size_t>(0, tree.rank('Y', 4), nullptr, LINE_INFO()); //U
        Assert::AreEqual<size_t>(0, tree.rank('Y', 5), nullptr, LINE_INFO()); //N
        Assert::AreEqual<size_t>(0, tree.rank('Y', 6), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('Y', 7), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(0, tree.rank('Y', 8), nullptr, LINE_INFO()); //G
        Assert::AreEqual<size_t>(0, tree.rank('Y', 9), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('Y', 10), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(0, tree.rank('Y', 11), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(0, tree.rank('Y', 12), nullptr, LINE_INFO()); //X
        Assert::AreEqual<size_t>(1, tree.rank('Y', 13), nullptr, LINE_INFO()); //Y
        Assert::AreEqual<size_t>(1, tree.rank('Y', 14), nullptr, LINE_INFO()); //
        Assert::AreEqual<size_t>(1, tree.rank('Y', 15), nullptr, LINE_INFO()); //A
        Assert::AreEqual<size_t>(1, tree.rank('Y', 16), nullptr, LINE_INFO()); //L
        Assert::AreEqual<size_t>(1, tree.rank('Y', 17), nullptr, LINE_INFO()); //P
        Assert::AreEqual<size_t>(1, tree.rank('Y', 18), nullptr, LINE_INFO()); //H
        Assert::AreEqual<size_t>(1, tree.rank('Y', 19), nullptr, LINE_INFO()); //A
    }

    TEST_METHOD(TestSelect) {
        auto tree = WaveletTree("SAMSUNG GALAXY ALPHA");
        Assert::AreEqual<size_t>(7, tree.select(' ', 1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(14, tree.select(' ', 2), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(1, tree.select('A', 1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(9, tree.select('A', 2), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(11, tree.select('A', 3), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(15, tree.select('A', 4), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(19, tree.select('A', 5), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(6, tree.select('G', 1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(8, tree.select('G', 2), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(18, tree.select('H', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(2, tree.select('M', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(5, tree.select('N', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(17, tree.select('P', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(0, tree.select('S', 1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(3, tree.select('S', 2), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(4, tree.select('U', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(12, tree.select('X', 1), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(13, tree.select('Y', 1), nullptr, LINE_INFO());

    }

    TEST_METHOD(TestAccess) {
        auto tree = WaveletTree("SAMSUNG GALAXY ALPHA");

        Assert::AreEqual<char>('S', tree.access(0), nullptr, LINE_INFO());
        Assert::AreEqual<char>('A', tree.access(1), nullptr, LINE_INFO());
        Assert::AreEqual<char>('M', tree.access(2), nullptr, LINE_INFO());
        Assert::AreEqual<char>('S', tree.access(3), nullptr, LINE_INFO());
        Assert::AreEqual<char>('U', tree.access(4), nullptr, LINE_INFO());
        Assert::AreEqual<char>('N', tree.access(5), nullptr, LINE_INFO());
        Assert::AreEqual<char>('G', tree.access(6), nullptr, LINE_INFO());
        Assert::AreEqual<char>(' ', tree.access(7), nullptr, LINE_INFO());
        Assert::AreEqual<char>('G', tree.access(8), nullptr, LINE_INFO());
        Assert::AreEqual<char>('A', tree.access(9), nullptr, LINE_INFO());
        Assert::AreEqual<char>('L', tree.access(10), nullptr, LINE_INFO());
        Assert::AreEqual<char>('A', tree.access(11), nullptr, LINE_INFO());
        Assert::AreEqual<char>('X', tree.access(12), nullptr, LINE_INFO());
        Assert::AreEqual<char>('Y', tree.access(13), nullptr, LINE_INFO());
        Assert::AreEqual<char>(' ', tree.access(14), nullptr, LINE_INFO());
        Assert::AreEqual<char>('A', tree.access(15), nullptr, LINE_INFO());
        Assert::AreEqual<char>('L', tree.access(16), nullptr, LINE_INFO());
        Assert::AreEqual<char>('P', tree.access(17), nullptr, LINE_INFO());
        Assert::AreEqual<char>('H', tree.access(18), nullptr, LINE_INFO());
        Assert::AreEqual<char>('A', tree.access(19), nullptr, LINE_INFO());
    }
    };
}