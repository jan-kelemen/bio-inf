#include "stdafx.h"
#include "CppUnitTest.h"

#include "../../RRRTable.h"
#include "../../RRRSequence.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace bioinf;

namespace bwtrrrutests {
    TEST_CLASS(RRRSequenceTest) {
public:
    TEST_METHOD(TestCoding3) {
        RRRTable::initialize_for_block_size(3);

        //111110111011101111111101111111010000010101101000011100001011001100

        //111 3 | 0 110
        //110 2 | 2 1010
        //111 3 | 0 110
        //011 2 | 0 1000
        //101 2 | 1 1001
        //111 3 | 0 110
        //111 3 | 0 110
        //101 2 | 1 1001
        //111 3 | 0 110
        //111 3 | 0 110
        //010 1 | 1 0101
        //000 0 | 0 000
        //010 1 | 1 0101
        //101 2 | 1 1001
        //101 2 | 1 1001
        //000 0 | 0 000
        //011 2 | 0 1000
        //100 1 | 2 0110
        //001 1 | 0 0100
        //011 2 | 0 1000
        //001 1 | 0 0100
        //100 1 | 2 0110

        //11010101101000100111011010011101100101000010110011001000100001100100100001000110

        auto s1 = RRRSequence("111110111011101111111101111111010000010101101000011100001011001100");
        auto c1 = std::string("11010101101000100111011010011101100101000010110011001000100001100100100001000110");
        Assert::AreEqual<std::string>(c1, to_string(s1), L"Coded sequence 1 mismatch", LINE_INFO());

        //110011000110011101000000111100100001000111100111100001111010100000

        //110 2 | 2 1010
        //011 2 | 0 1000
        //000 0 | 0 000
        //110 2 | 2 1010
        //011 2 | 0 1000
        //101 2 | 1 1001
        //000 0 | 0 000
        //000 0 | 0 000
        //111 3 | 0 110
        //100 1 | 2 0110
        //100 1 | 2 0110
        //001 1 | 0 0100
        //000 0 | 0 000
        //111 3 | 0 110
        //100 1 | 2 0110
        //111 3 | 0 110
        //100 1 | 2 0110
        //001 1 | 0 0100
        //111 3 | 0 110
        //010 1 | 1 0101
        //100 1 | 2 0110
        //000 0 | 0 000

        //1010100000010101000100100000011001100110010000011001101100110010011001010110000

        auto s2 = RRRSequence("110011000110011101000000111100100001000111100111100001111010100000");
        auto c2 = std::string("1010100000010101000100100000011001100110010000011001101100110010011001010110000");
        Assert::AreEqual<std::string>(c2, to_string(s2), L"Coded sequence 2 mismatch", LINE_INFO());

        //000101101100011101011001111101100100001000101100010010111010001101

        //000 0 | 0 000
        //101 2 | 1 1001
        //101 2 | 1 1001
        //100 1 | 2 0110
        //011 2 | 0 1000
        //101 2 | 1 1001
        //011 2 | 0 1000
        //001 1 | 0 0100
        //111 3 | 0 110
        //101 2 | 1 1001
        //100 1 | 2 0110
        //100 1 | 2 0110
        //001 1 | 0 0100
        //000 0 | 0 000
        //101 2 | 1 1001
        //100 1 | 2 0110
        //010 1 | 1 0101
        //010 1 | 1 0101
        //111 3 | 0 110
        //010 1 | 1 0101
        //001 1 | 0 0100
        //101 2 | 1 1001

        //000100110010110100010011000010011010010110011001000001001011001010101110010101001001

        auto s3 = RRRSequence("000101101100011101011001111101100100001000101100010010111010001101");
        auto c3 = std::string("000100110010110100010011000010011010010110011001000001001011001010101110010101001001");
        Assert::AreEqual<std::string>(c3, to_string(s3), L"Coded sequence 3 mismatch", LINE_INFO());
    }

    TEST_METHOD(TestLengths3) {
        RRRTable::initialize_for_block_size(3);
        // Same data as previous tests

        auto s1 = RRRSequence("111110111011101111111101111111010000010101101000011100001011001100");
        Assert::AreEqual<uint8_t>(3, s1.block_length(), L"Block length doesn't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(12, s1.blocks_in_superblock(), L"Blocks in superblock don't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(2, s1.class_length(), L"Class lengths don't match", LINE_INFO());
        Assert::AreEqual<size_t>(66, s1.sequence_length(), L"Sequence length doesn't match", LINE_INFO());

        auto s2 = RRRSequence("110011000110011101000000111100100001000111100111100001111010100000");
        Assert::AreEqual<uint8_t>(3, s2.block_length(), L"Block length doesn't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(12, s2.blocks_in_superblock(), L"Blocks in superblock don't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(2, s2.class_length(), L"Class lengths don't match", LINE_INFO());
        Assert::AreEqual<size_t>(66, s2.sequence_length(), L"Sequence length doesn't match", LINE_INFO());

        auto s3 = RRRSequence("000101101100011101011001111101100100001000101100010010111010001101");
        Assert::AreEqual<uint8_t>(3, s3.block_length(), L"Block length doesn't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(12, s3.blocks_in_superblock(), L"Blocks in superblock don't match", LINE_INFO());
        Assert::AreEqual<uint8_t>(2, s3.class_length(), L"Class lengths don't match", LINE_INFO());
        Assert::AreEqual<size_t>(66, s3.sequence_length(), L"Sequence length doesn't match", LINE_INFO());
    }

    TEST_METHOD(TestRanks3) {
        RRRTable::initialize_for_block_size(3);

        //1 1  0
        //1 2  0
        //1 3  0
        //1 4  0
        //1 5  0
        //0 5  1
        //1 6  1
        //1 7  1
        //1 8  1
        //0 8  2
        //1 9  2
        //1 10 2
        //1 11 2
        //0 11 3
        //1 12 3
        //1 13 3
        //1 14 3
        //1 15 3
        //1 16 3
        //1 17 3
        //1 18 3
        //1 19 3
        //0 19 4
        //1 20 4
        //1 21 4
        //1 22 4
        //1 23 4
        //1 24 4
        //1 25 4
        //1 26 4
        //0 26 5
        //1 27 5
        auto s1 = RRRSequence("11111011101110111111110111111101");
        Assert::AreEqual<size_t>(1, s1.rank1(0), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank1(1), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank1(2), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(3), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.rank1(4), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.rank1(5), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.rank1(6), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(7, s1.rank1(7), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(8, s1.rank1(8), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(8, s1.rank1(9), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(9, s1.rank1(10), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(10, s1.rank1(11), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(11, s1.rank1(12), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(11, s1.rank1(13), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(12, s1.rank1(14), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(13, s1.rank1(15), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(14, s1.rank1(16), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(15, s1.rank1(17), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(16, s1.rank1(18), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(17, s1.rank1(19), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(18, s1.rank1(20), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(19, s1.rank1(21), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(19, s1.rank1(22), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(20, s1.rank1(23), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(21, s1.rank1(24), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(22, s1.rank1(25), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(23, s1.rank1(26), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(24, s1.rank1(27), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(25, s1.rank1(28), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(26, s1.rank1(29), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(26, s1.rank1(30), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(27, s1.rank1(31), L"Rank1 differs", LINE_INFO());

        Assert::AreEqual<size_t>(0, s1.rank0(0), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(0, s1.rank0(1), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(0, s1.rank0(2), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(0, s1.rank0(3), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(0, s1.rank0(4), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank0(5), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank0(6), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank0(7), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank0(8), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank0(9), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank0(10), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank0(11), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank0(12), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(13), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(14), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(15), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(16), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(17), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(18), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(19), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(20), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank0(21), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(22), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(23), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(24), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(25), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(26), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(27), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(28), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank0(29), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.rank0(30), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.rank0(31), L"Rank1 differs", LINE_INFO());
    }

    TEST_METHOD(TestRank2) {
        RRRTable::initialize_for_block_size(2);

        auto s1 = RRRSequence("10011100000011000100");
        Assert::AreEqual<size_t>(1, s1.rank1(0), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank1(1), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.rank1(2), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.rank1(3), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.rank1(4), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(5), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(6), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(7), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(8), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(9), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(10), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.rank1(11), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.rank1(12), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.rank1(13), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.rank1(14), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.rank1(15), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.rank1(16), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(7, s1.rank1(17), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(7, s1.rank1(18), L"Rank1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(7, s1.rank1(19), L"Rank1 differs", LINE_INFO());

        //Assert::AreEqual<size_t>(0, s1.rank0(0), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(0, s1.rank0(1), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(0, s1.rank0(2), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(0, s1.rank0(3), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(0, s1.rank0(4), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(1, s1.rank0(5), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(1, s1.rank0(6), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(1, s1.rank0(7), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(1, s1.rank0(8), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(2, s1.rank0(9), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(2, s1.rank0(10), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(2, s1.rank0(11), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(2, s1.rank0(12), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(13), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(14), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(15), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(16), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(17), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(18), L"Rank1 differs", LINE_INFO());
        //Assert::AreEqual<size_t>(3, s1.rank0(19), L"Rank1 differs", LINE_INFO());
    }

    TEST_METHOD(TestSelect3) {
        RRRTable::initialize_for_block_size(3);
        auto s1 = RRRSequence("11111011101110111111110111111101");
        Assert::AreEqual<size_t>(0, s1.select1(1), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(1, s1.select1(2), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.select1(3), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.select1(4), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.select1(5), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.select1(6), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(7, s1.select1(7), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(8, s1.select1(8), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(10, s1.select1(9), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(11, s1.select1(10), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(12, s1.select1(11), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(14, s1.select1(12), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(15, s1.select1(13), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(16, s1.select1(14), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(17, s1.select1(15), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(18, s1.select1(16), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(19, s1.select1(17), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(20, s1.select1(18), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(21, s1.select1(19), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(23, s1.select1(20), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(24, s1.select1(21), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(25, s1.select1(22), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(26, s1.select1(23), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(27, s1.select1(24), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(28, s1.select1(25), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(29, s1.select1(26), L"Select1 differs", LINE_INFO());
        Assert::AreEqual<size_t>(31, s1.select1(27), L"Select1 differs", LINE_INFO());

        Assert::AreEqual<size_t>(5, s1.select0(1), L"Select0 differs", LINE_INFO());
        Assert::AreEqual<size_t>(9, s1.select0(2), L"Select0 differs", LINE_INFO());
        Assert::AreEqual<size_t>(13, s1.select0(3), L"Select0 differs", LINE_INFO());
        Assert::AreEqual<size_t>(22, s1.select0(4), L"Select0 differs", LINE_INFO());
        Assert::AreEqual<size_t>(30, s1.select0(5), L"Select0 differs", LINE_INFO());
    }

    TEST_METHOD(TestSelect1) {
        RRRTable::initialize_for_block_size(1);

        auto s1 = RRRSequence("1011011");
        Assert::AreEqual<size_t>(0, s1.select1(1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(2, s1.select1(2), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(3, s1.select1(3), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(5, s1.select1(4), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(6, s1.select1(5), nullptr, LINE_INFO());

        Assert::AreEqual<size_t>(1, s1.select0(1), nullptr, LINE_INFO());
        Assert::AreEqual<size_t>(4, s1.select0(2), nullptr, LINE_INFO());
    }

    TEST_METHOD(TestAccess3) {
        RRRTable::initialize_for_block_size(3);
        auto s1 = RRRSequence("11111011101110111111110111111101");
        Assert::AreEqual<char>(1, s1.access(0), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(1), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(2), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(3), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(4), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(0, s1.access(5), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(6), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(7), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(8), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(0, s1.access(9), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(10), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(11), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(12), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(0, s1.access(13), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(14), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(15), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(16), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(17), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(18), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(19), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(20), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(21), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(0, s1.access(22), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(23), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(24), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(25), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(26), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(27), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(28), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(29), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(0, s1.access(30), L"Access differs", LINE_INFO());
        Assert::AreEqual<char>(1, s1.access(31), L"Access differs", LINE_INFO());
    }
    };
}