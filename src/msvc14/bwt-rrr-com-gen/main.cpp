#include <iostream>
#include <fstream>
#include <random>
#include <set>
#include <string>
#include <map>

std::string parse_fasta(char const* filename);

std::map<char, int> select_statistics(std::string const& sequence);

std::map<char, std::uniform_int_distribution<size_t>> select_distributions(std::map<char, int> const& statistics);

std::set<char> alphabet(std::map<char, int> const& statistics);

//PLEASE DON'T USE THIS - POSSIBLE BUGS
// Jan Kelemen
int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cout << "Invalid number of arguments\n";
        std::cout << "First argument is the filename with the sequence\n";
        std::cout << "Second argument is the output filename\n";
        std::cout << "Third argument is the number of commands to generate\n";
        exit(EXIT_FAILURE);
    }

    auto command_count = std::atoi(argv[3]);

    auto sequence = parse_fasta(argv[1]);
    auto stats = select_statistics(sequence);
    auto alpha = alphabet(stats);


    std::random_device rd;
    std::mt19937 gen(rd());

    std::uniform_int_distribution<int> command_dist(0, 2);
    std::uniform_int_distribution<int> alpha_dist('A', 'A' + alpha.size() - 1);
    std::uniform_int_distribution<size_t> len_dist(0, sequence.length() - 1);
    auto select_dists = select_distributions(stats);

    std::ofstream output(argv[2]);
    output << "MEM\n";
    for (auto i = 0; i < command_count; ++i) {
        switch (command_dist(gen)) {
            case 0:
                output << "rank " << char(alpha_dist(gen)) << ' ' << len_dist(gen) << '\n';
                break;
            case 1:
                output << "access " << len_dist(gen) << '\n';
                break;
            case 2:
            {
                auto c = alpha_dist(gen);
                output << "select " << char(c) << ' ' << select_dists[c](gen) << '\n';
                break;
            }

            default:
                break;
        }
    }
    output << "STAT\nMEM\nEXIT\n";
}

std::string parse_fasta(char const* filename) {
    std::ifstream file(filename);
    std::string content;
    std::string line;
    std::getline(file, line); // ignore first line with block name
    while (std::getline(file, line)) {
        if (line[0] == ',') { continue; } //skip comments
        line.erase(line.find_last_not_of(" \n\r\t") + 1);
        content += line;
    }

    return content;
}

std::map<char, int> select_statistics(std::string const& sequence) {
    auto rv = std::map<char, int>();
    for (auto c : sequence) {
        ++rv[c];
    }
    return rv;
}

std::map<char, std::uniform_int_distribution<size_t>> select_distributions(std::map<char, int> const& statistics) {
    auto rv = std::map<char, std::uniform_int_distribution<size_t>>();
    for (auto p : statistics) {
        rv[p.first] = std::uniform_int_distribution<size_t>(1, p.second);
    }
    return rv;
}

std::set<char> alphabet(std::map<char, int> const& statistics) {
    auto rv = std::set<char>();
    for (auto p : statistics) {
        rv.insert(p.first);
    }
    return rv;
}
