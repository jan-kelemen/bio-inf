#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <string>

// Jan Kelemen
int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cout << "Invalid number of arguments\n";
        std::cout << "First argument is the number of characters used for sequence generation\n";
        std::cout << "Second argument is the length of the sequence\n";
        exit(EXIT_FAILURE);
    }

    auto characters = std::stoi(argv[1]);
    auto length = std::atoi(argv[2]);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<size_t> dis(0, characters - 1);

    std::string sequence;
    sequence.reserve(length);

    for (auto i = 0; i < length; ++i) {
        sequence.push_back('A' + dis(gen));
    }

    auto filename = std::to_string(characters) + "_" + std::to_string(length) + ".txt";
    std::ofstream output(filename.c_str());

    output << '>' << filename << '\n';
    auto cstr = sequence.c_str();

    for (size_t i = 0; i <= sequence.length() / 60 && i * 60 != sequence.length(); ++i) {
        output.write(cstr + i * 60, std::min<size_t>(60, sequence.length() - i * 60));
        output << '\n';
    }
}