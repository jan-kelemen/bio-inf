#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

std::string parse_fasta(char const* filename);

size_t rank(std::string::const_iterator begin, std::string::const_iterator end, char c, size_t i);

size_t select(std::string::const_iterator begin, std::string::const_iterator end, char c, size_t i);

char access(std::string::const_iterator begin, std::string::const_iterator end, size_t i);


// PLEASE DON'T USE THIS - POSSIBLE BUGS
// Jan Kelemen
int main(int argc, char *argv[]) {
    if (argc != 4) {
        std::cout << "Invalid number of arguments\n";
        std::cout << "First argument is the filename with the sequence\n";
        std::cout << "Second argument is the commands filename\n";
        std::cout << "Third argument is the output filename\\n";
        exit(EXIT_FAILURE);
    }

    auto sequence = parse_fasta(argv[1]);
    std::ifstream commands(argv[2]);
    std::ofstream output(argv[3]);
   
    std::string line;
    while (std::getline(commands, line)) {
        if (line[0] == 'M') {
            //MEM
            continue;
        }
        if (line[0] == 'S') {
            //STATS
            continue;
        }
        if (line[0] == 'E') {
            //EXIT
            break;
        }

        std::istringstream stream(line);
        std::string ignore;
        stream >> ignore;
        if (line[0] == 'r') {
            //rank

            char c;
            size_t i;
            stream >> c >> i;

            output << rank(sequence.begin(), sequence.end(), c, i) << '\n';
        }
        else if (line[0] == 's') {
            //select

            char c;
            size_t i;
            stream >> c >> i;

            output << select(sequence.begin(), sequence.end(), c, i) << '\n';
        }
        else {
            //access

            size_t i;
            stream >> i;

            output << access(sequence.begin(), sequence.end(), i) << '\n';
        }
    }
}

std::string parse_fasta(char const* filename) {
    std::ifstream file(filename);
    std::string content;
    std::string line;
    std::getline(file, line); // ignore first line with block name
    while (std::getline(file, line)) {
        if (line[0] == ',') { continue; } //skip comments
        line.erase(line.find_last_not_of(" \n\r\t") + 1);
        content += line;
    }

    return content;
}

size_t rank(std::string::const_iterator begin, std::string::const_iterator end, char c, size_t i) {
    return std::count(begin, begin + i + 1, c);
}

size_t select(std::string::const_iterator begin, std::string::const_iterator end, char c, size_t i) {
    auto count = 0;
    for (auto it = begin; it != end; ++it) {
        if (*it == c) {
            ++count;
            if (count == i) {
                return std::distance(begin, it);
            }
        }
    }
    return -1;
}

char access(std::string::const_iterator begin, std::string::const_iterator end, size_t i) {
    return *(begin + i);
}