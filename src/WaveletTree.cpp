#include "WaveletTree.h"

#include <algorithm>
#include <array>
#include <iterator>
#include <set>

#include <iostream>

#include "WaveletNode.h"
#include "RRRTable.h"

namespace bioinf {
    WaveletTree::WaveletTree(std::string const& sequence) {
        std::fill(alphabet_indexes_.begin(), alphabet_indexes_.end(), -1);
        for (auto c : sequence) {
            alphabet_indexes_[c] = 1;
        }
        for (auto i = 0; i < alphabet_indexes_.size(); ++i) {
            if (alphabet_indexes_[i] == 1) {
                alphabet_.push_back(i);
                alphabet_indexes_[i] = i;
            }
        }

        //initialize RRR table for the biggest block size
        auto block_size_ = static_cast<int8_t>(std::floor(std::log2(sequence.length()) / 2));
        RRRTable::initialize_for_block_size(block_size_);

        try {
            root_ = new WaveletNode(sequence, alphabet_, 0, alphabet_.size());
        }
        catch (std::bad_alloc&) {
            delete root_;
            throw;
        }
    }

    size_t WaveletTree::rank(char c, size_t index) const {
        auto const* v = root_;
        auto const* root = v;
        auto r = index;
        
        //TODO: possible memoization to avoid runtime overhead
        auto it = std::lower_bound(alphabet_.begin(), alphabet_.end(), c);
        size_t char_index = std::distance(alphabet_.begin(), it);

        while (v != nullptr) {
            if (v != root) {
                --r;
            }

            if (char_index < v->zero_encoded_to_index()) {
                r = v->rank0(r);
                v = v->left();
            }
            else {
                r = v->rank1(r);
                v = v->right();
            }

            if (r == 0) { return 0; }
        }
        return r;
    }

    size_t WaveletTree::select(char c, size_t count) const {
        //TODO: possible memoization to avoid runtime overhead
        auto it = std::lower_bound(alphabet_.begin(), alphabet_.end(), c);
        size_t char_index = std::distance(alphabet_.begin(), it);

        auto const* v = node_for_symbol(c, char_index);
        auto r = char_index < v->zero_encoded_to_index() ? v->select0(count) : v->select1(count);

        while (v != root_) {
            ++r;
            auto const* parent = v->parent();
            r = v == parent->left() ? parent->select0(r) : parent->select1(r);

            v = parent;
        }

        return r;
    }

    char WaveletTree::access(size_t index) const {
        auto const* v = root_;
        auto const* root = v;
        auto r = index;

        while (v != nullptr) {
            if (v != root) {
                --r;
            }

            if (v->access(r) == 0) {
                r = v->rank0(r);
                if (v->left() == nullptr) {
                    return alphabet_[v->alphabet_begin_index()];
                }
                v = v->left();
            }
            else {
                r = v->rank1(r);
                if (v->right() == nullptr) {
                    return alphabet_[v->alphabet_end_index()];
                }
                v = v->right();
            }
        }

        return 0;
    }

    WaveletTree::~WaveletTree() {
        delete root_;
    }

    WaveletNode const* WaveletTree::node_for_symbol(char c, size_t const& char_index) const {
        auto const* v = root_;
        auto const* temp = v;

        while (temp != nullptr) {
            v = temp;
            if (char_index < temp->zero_encoded_to_index()) {
                temp = temp->left();
            }
            else {
                temp = temp->right();
            }
        }
        return v;
    }
}

